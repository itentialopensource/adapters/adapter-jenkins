
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:14PM

See merge request itentialopensource/adapters/adapter-jenkins!15

---

## 0.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-jenkins!13

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:28PM

See merge request itentialopensource/adapters/adapter-jenkins!12

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:31PM

See merge request itentialopensource/adapters/adapter-jenkins!11

---

## 0.4.0 [05-09-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!10

---

## 0.3.3 [03-26-2024]

* Changes made at 2024.03.26_14:25PM

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!9

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_13:23PM

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!8

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:48PM

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!7

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!6

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!5

---

## 0.1.7 [08-04-2021]

- Redo the add of generic path vars

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!4

---

## 0.1.6 [08-04-2021]

- Add more path variables on generic requests

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!3

---

## 0.1.5 [03-07-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!2

---

## 0.1.4 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-jenkins!1

---

## 0.1.3 [01-09-2020] & 0.1.2 [11-22-2019] & 0.1.1 [11-22-2019]

- Initial Commit

See commit 13722dd

---
