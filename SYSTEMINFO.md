# Jenkins

Vendor: Jenkins
Homepage: https://www.jenkins.io/

Product: Jenkins
Product Page: https://www.jenkins.io/

## Introduction
We classify Jenkins into the CI/CD domain since it is used for continuous integration and continuous delivery (CI/CD) pipelines in software development.

## Why Integrate
The Jenkins adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Jenkins. With this adapter you have the ability to perform operations such as:

- Build Pipeline
- Get Job Details

## Additional Product Documentation
The [API documents for Jenkins](https://wiki.jenkins-ci.org/display/JENKINS/Remote+access+API)